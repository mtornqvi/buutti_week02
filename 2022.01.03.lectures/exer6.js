const arrA = [7, 2, 3, 4];
const arrB = [3, 4, 5, 6];

const combArray = arrA.concat(arrB);

//
const uniqArray = combArray.reduce ( (a,b) => {
    if (a.indexOf(b) <0) { // if b is not found among already pushed => push it
        a.push(b);
    }
    return a;
},
[] // do nothing in reducer
);

console.log(uniqArray);