function convertFirstLetters(inputString) {

    let stringArray = inputString.trim().split(" ");
    let outputString = "";
    
    for (let string of stringArray) {
        outputString = outputString + string[0].toUpperCase() + string.substring(1) + " ";
    }

    console.log(outputString);

}

convertFirstLetters("write a function that takes in a string");
convertFirstLetters(" 12 method returns the part of the string between the start and end");