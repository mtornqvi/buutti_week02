function factorialize(num) {
    if (num < 0 || !Number.isInteger(num)) 
        return -1;
    else if (num == 0) 
        return 1;
    else {
        return (num * factorialize(num - 1));
    }
}
console.log(`Test factorialize(-1) : ${factorialize(-1)}.`);
console.log(`Test factorialize(0.21) : ${factorialize(0.21)}.`);
console.log(`Test factorialize(0) : ${factorialize(0)}.`);
console.log(`Test factorialize(5) : ${factorialize(5)}.`);
console.log(`Test factorialize(6) : ${factorialize(6)}.`);