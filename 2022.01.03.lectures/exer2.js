function getRandomInt(min,max) {
    return Math.round(min + Math.random() * (max-min));
}

console.log("Test one:");
for (let i=0;i<20;i++) {
    console.log(getRandomInt(2,5));
}
console.log("Test two:");
for (let i=0;i<20;i++) {
    console.log(getRandomInt(3,9));
}
