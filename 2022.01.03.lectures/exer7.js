function getRandomInt(min,max) {
    return Math.round(min + Math.random() * (max-min));
}

const numbers = [];

while (numbers.length < 7) {
    let num = getRandomInt(1,40);
    if (numbers.indexOf(num) < 0) { // must be unique
        numbers.push(num);
    }
}

console.log("Numbers are:");
numbers.forEach((n,i) => setTimeout(() => {console.log(n);},i*1000));

