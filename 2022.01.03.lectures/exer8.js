const fibonacci = (n) => {

    let fib = [0,1];
    let data = [1];
  

    for(let i = 2; i <= n; i++) {
        fib[i] = fib[i - 1] + fib[i - 2]; 
        data.push(fib[i]);
    }
    
    return data;
};



console.log(fibonacci(1));
console.log(fibonacci(2));
console.log(fibonacci(5));
console.log(fibonacci(12));