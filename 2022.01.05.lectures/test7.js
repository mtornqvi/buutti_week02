function Car (make,model, year) {
    this.make = make;
    this.model = model;
    this.year = year;
    this.velocity = 100;
}

// säästää muistia
Car.prototype.break = function () {
    this.velocity = 0;    
};

const audi = new Car ("audi","tt", "2001");

console.log(Car.prototype);
console.log(typeof Car.prototype);
