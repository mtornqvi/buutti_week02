const cat = {
    name : "Mirre",
    age : 15
};

const copyCat = {
    ...cat,
    name : "Keijo", // vanhojen arvojen muuttaminen myös mahdollista
    fur : "thick"
};

console.log(copyCat);
