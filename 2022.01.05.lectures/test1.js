const car = {
    brand : "Subaru",
    model : "Legacy",
    engine : "2.0T"
};

console.error(car);
console.error(car.brand);

car.engine = "2.5T";
console.error(car);

car.engine = { 
    type: "boxer-6",
    displacement : "2.0"
};

console.log(car);
