// Lista objekteja

const cars = [ {brand:"audi"},{brand:"subaru"}];


// spread-syntaxilla kopio ensiksi
const updatedCars = cars.map( (car) => ({ ...car, color: "black"}));

console.log(cars);
console.log(updatedCars);
