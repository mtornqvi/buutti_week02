function Car (make,model, year) {
    this.make = make;
    this.model = model;
    this.year = year;
    this.velocity = 100;
}

const audi = new Car ("audi","tt", "2001");
const subaru = new Car("subary","legacy","2000");

// pitää käyttää alaviivoilla määrittely!
audi.__proto__.break = function () {
    this.velocity = 0;
};

console.log(audi.color);
