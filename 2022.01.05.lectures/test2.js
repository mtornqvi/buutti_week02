// Lista objekteja
// varoittava esimerkki

const cars = [ {brand:"audi"},{brand:"subaru"}];

// käy helposti : ei tehdä uutta => voi tulla onglemia

const updatedCars = cars.map( (car) => car.color = "black");

console.log(cars);
console.log(updatedCars);
