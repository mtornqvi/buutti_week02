// npm init -y
// modify : package.json
// "type": "module"

/**
 * Print out word for word, and every time the word you’re trying to print is “joulu”, you replace
that word with “kinkku”, and every time your printing “lapsilla” you print “poroilla”.
 */

import fs, { read } from "fs";

const readStream = fs.createReadStream("./textFile.txt", "utf-8");
const writeStream = fs.createWriteStream("./textFile2.txt", "utf-8");

// ei välttämättä toimi isoilla tiedostoilla
readStream.on("data", (txt) => {
    
    let regEx = new RegExp("joulu","ig"); // ignore case, global
    txt = txt.replaceAll(regEx,"kinkku");

    regEx = new RegExp("lapsilla","ig"); // ignore case, global
    txt = txt.replaceAll(regEx,"poroilla");

    console.log(txt);

    writeStream.write(txt,  "utf-8", (err) => {
        if (err) console.log(err);
        else console.log("success");
    });
});


