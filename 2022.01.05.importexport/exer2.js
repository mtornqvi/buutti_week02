import fs from "fs";

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

const file = "forecast_data.json";


// can have an extra callback function 
// data should be parsed with callback function
const readJSONDataFromFile = (file,callback) => {
    fs.readFile(file,"utf-8", (err,file) => {
        if (err) {
            console.log(err);
        } else {
            try {
                callback(JSON.parse(file));        
            } catch(error) {
                console.log(error.message);
            }
        }

    }
    );
};

// JSON should be stringified before writing, writefile has a callback function
const saveJSONDataToFile = (file,data) => {
    fs.writeFile(file,JSON.stringify(data), "utf-8", (err) => {
        if (err) {
            console.log("Couldn't save to file.");
        }
    });
};

saveJSONDataToFile(file,forecast);

// define callback function here
readJSONDataFromFile(file,(JSONObject) => {
    JSONObject.temperature = 15;
    saveJSONDataToFile(file,JSONObject);
});

