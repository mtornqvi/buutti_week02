/**
 * Return the number (count) of vowels in the given string. Let's consider a, e, i, o, u, y as vowels in
this exercise.
4.5
 *  getVowelCount('abracadabra') // 5
 */



function getVowelCount(inputString) {

    const strArray = String(inputString).toUpperCase().split("");
    const vowels = ["A","E","I","O","U","Y"];
    const resultArray = [];


    for (let i=0; i<=vowels.length; i++) {

        let char = vowels[i];
        let count = strArray.reduce(
            (previousValue, currentValue) => 
                previousValue + ((currentValue === char) ? 1 : 0),0);
        resultArray[i] = {char, count};

    }

    const result = resultArray.reduce(
        (previousValue, currentValue) => 
            previousValue + currentValue.count,0);

    return result;
}



console.log(getVowelCount("abracadabra")); // 5
console.log(getVowelCount("equality")); // 5
console.log(getVowelCount("satisfies")); // 4
