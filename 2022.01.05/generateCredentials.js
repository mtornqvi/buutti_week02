/**
Create a function (or multiple functions) that generates username and password from given firstname
and lastname.
Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in
lower case
Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase +
random special character + last 2 numbers from current year
Example: John Doe -> B20dojo, mjE(20
generateCredentials("John", "Doe")
Get to know to ASCII table. Random letters and special characters can be searched from ASCII table with
String.fromCharCode() method with indexes. For example String.fromCharCode(65) returns letter A.
Hints: Generate random numbers (indexes) to get one random letter and one special character. Use range
of 65 to 90 to get the LETTER and 33 to 47 to get the SPECIAL CHARACTER. Use build-in function to get
the current year
Output: “username: username, password: password”
 */

function getRandomInt(min,max) {
    return Math.round(min + Math.random() * (max-min));
}

function generateCredentials(firstName, lastName) {

    const currentYear = new Date().getFullYear().toString().substring(2); // only last two digits

    let username = "B" + currentYear + lastName.substring(0,2).toLowerCase() + firstName.substring(0,2).toLowerCase();
    
    // ASCII : 65-90 => letter
    // ASCII : 33-47 => special character
    let password = 
    String.fromCharCode(getRandomInt(65,90)) + firstName.substring(0,1).toLowerCase() + 
    lastName.substring(lastName.length-1).toLowerCase() + String.fromCharCode(getRandomInt(33,47)) + currentYear;

    return {username,password};
}



console.log(generateCredentials("John", "Doe")); 
