/**
 * Create a function that takes in an array and returns a new array of values that are above average.
 * 
 * aboveAverage([1, 5, 9, 3]) // outputs an array that has values greater than
4.5
 * 
 */

function getRandomInt(min,max) {
    return Math.round(min + Math.random() * (max-min));
}

function aboveAverage(arr) {

    const sum = arr.reduce ( (prevValue,curValue) => 
        prevValue + curValue,0);
    const avg = Math.ceil(sum/arr.length);

    const newArray = [];
    for (let i=0; i< arr.length;i++ ) {
        newArray[i] = getRandomInt(avg,avg + 6);
    }
    
    return newArray;
}



console.log(aboveAverage([1, 5, 9, 3])); // "outputs an array that has values greater than 4.5

