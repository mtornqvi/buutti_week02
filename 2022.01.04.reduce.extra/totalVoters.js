// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
/**
 * The reduce() method executes a user-supplied “reducer” 
 * callback function on each element of the array, in order, passing in the return value from the calculation 
 * on the preceding element. The final result of running the reducer across all elements of the array is a single value. 
 * 
 * The first time that the callback is run there is no "return value of the previous calculation". If supplied, 
 *  an initial value may be used in its place. Otherwise array element 0 is used as the initial value and 
 * iteration starts from the next element (index 1 instead of index 0). 
 */

// Arrow function
// reduce((previousValue, currentValue) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ }, initialValue);


function totalVotes(arr) {
    return arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + ((currentValue.voted === true) ? 1 : 0), 0 ); // requires Intialvalue to work!!
}

const voters = [
    {name:"Bob" , age: 30, voted: true},
    {name:"Jake" , age: 32, voted: true},
    {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false},
    {name:"Phil" , age: 21, voted: true},
    {name:"Ed" , age:55, voted:false},
    {name:"Tami" , age: 54, voted:true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

console.log(totalVotes(voters)); 