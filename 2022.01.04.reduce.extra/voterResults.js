// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
/**
 * The reduce() method executes a user-supplied “reducer” 
 * callback function on each element of the array, in order, passing in the return value from the calculation 
 * on the preceding element. The final result of running the reducer across all elements of the array is a single value. 
 * 
 * The first time that the callback is run there is no "return value of the previous calculation". If supplied, 
 *  an initial value may be used in its place. Otherwise array element 0 is used as the initial value and 
 * iteration starts from the next element (index 1 instead of index 0). 
 */

// Arrow function
// reduce((previousValue, currentValue) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ }, initialValue);

/**
 Include how many of the potential voters were in the ages 18-25, 
 how many from 26-35, how many from 36-55, and how many of each of those age ranges actually voted. 
 The resulting object containing this data should have 6 properties. See the example output at the bottom.
 */

function voterResults(arr) {

    const numYoungVotes = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + (((currentValue.age <= 25) && currentValue.voted) ? 1 : 0), 0 ); // requires Intialvalue to work!!

    const numYoungPeople = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + ((currentValue.age <= 25) ? 1 : 0), 0 ); // requires Intialvalue to work!!

    const numMidVotesPeople = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + (((currentValue.age >= 26) && (currentValue.age <= 35) && currentValue.voted) ? 1 : 0), 0 ); // requires Intialvalue to work!!

    const numMidsPeople = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + ((currentValue.age >= 26) && (currentValue.age <= 35) ? 1 : 0), 0 ); // requires Intialvalue to work!!

    const numOldVotesPeople = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + (((currentValue.age >= 36) && (currentValue.age <= 55) && currentValue.voted) ? 1 : 0), 0 ); // requires Intialvalue to work!!
        
    const numOldsPeople = arr.reduce( 
        (previousValue, currentValue) => 
            previousValue + ((currentValue.age >= 36) && (currentValue.age <= 55) ? 1 : 0), 0 ); // requires Intialvalue to work!!
        
                    
    const resultArray = [
        {"numYoungVotes": numYoungVotes},
        {"numYoungPeople": numYoungPeople},
        {"numMidVotesPeople": numMidVotesPeople},
        {"numMidsPeople": numMidsPeople},
        {"numOldVotesPeople": numOldVotesPeople},
        {"numOldsPeople": numOldsPeople}
    
    ];

    return resultArray;
}

const voters = [
    {name:"Bob" , age: 30, voted: true},
    {name:"Jake" , age: 32, voted: true},
    {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false},
    {name:"Phil" , age: 21, voted: true},
    {name:"Ed" , age:55, voted:true},
    {name:"Tami" , age: 54, voted:true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/