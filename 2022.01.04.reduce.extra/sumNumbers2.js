// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
/**
 * The reduce() method executes a user-supplied “reducer” 
 * callback function on each element of the array, in order, passing in the return value from the calculation 
 * on the preceding element. The final result of running the reducer across all elements of the array is a single value. 
 * 
 * The first time that the callback is run there is no "return value of the previous calculation". If supplied, 
 *  an initial value may be used in its place. Otherwise array element 0 is used as the initial value and 
 * iteration starts from the next element (index 1 instead of index 0). 
 */

function total(arr) {
    const reducer = (previousValue, currentValue) => previousValue + currentValue;
    return arr.reduce(reducer,0);
}


console.log(total([1,2,3])); 