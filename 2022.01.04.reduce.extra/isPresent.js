// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
/**
 * The reduce() method executes a user-supplied “reducer” 
 * callback function on each element of the array, in order, passing in the return value from the calculation 
 * on the preceding element. The final result of running the reducer across all elements of the array is a single value. 
 * 
 * The first time that the callback is run there is no "return value of the previous calculation". If supplied, 
 *  an initial value may be used in its place. Otherwise array element 0 is used as the initial value and 
 * iteration starts from the next element (index 1 instead of index 0). 
 */

// Arrow function
// reduce((previousValue, currentValue) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ } );
// reduce((previousValue, currentValue, currentIndex, array) => { /* ... */ }, initialValue);



const isPresent = function(inputString, target) {

    const strArray = String(inputString).toLowerCase().split("");
    const targetChar = target.toLowerCase();

    if(targetChar.length !== 1) {
        console.error("Error : target char length is not 1.");
        process.exit(0);
    }

    let count = strArray.reduce(
        (previousValue, currentValue) => 
            previousValue + ((currentValue === targetChar) ? 1 : 0),0);
    
    if (count > 0) 
        return true;
    else 
        return false;
};



console.log(isPresent("abcd", "b"));
console.log(isPresent("efghi", "a"));
console.log(isPresent("efghi", "ab"));