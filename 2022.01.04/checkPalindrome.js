/** Check if given string is a palindrome.
Examples:
node .\checkPalindrome.js saippuakivikauppias -> Yes, 'saippuakivikauppias' is a
palindrome
node .\checkPalindrome.js saippuakäpykauppias -> No, 'saippuakäpykauppias' is not
a palindrome
 */


const n = process.argv.length;
if (n !== 3) {
    console.error(`Incorrect amount of arguments : ${n-2}. Correct usage example : node checkPalindrome.js saippuakivikauppias.  `);
    process.exit(0);
}

const word = process.argv[2].toLowerCase();
// note : must first split a word string to a word array so it can be processed  
const reversedWord = word.split("").reverse().join("");

if (word === reversedWord) {
    console.log(`Yes, ${word} is a palindrome`);
}
else {
    console.log(`No, ${word} is not a palindrome`);
}
