/** Write a program that takes in any two numbers from the command line, start and end. The program
creates and prints an array filled with numbers from start to end.

Examples:
node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]
Note the order of the values. When start is smaller than end, the order is ascending and when start is
greater than end, order is descending.
 */


const n = process.argv.length;
if (n !== 4) {
    console.error(`Incorrect amount of arguments : ${n-2}. Correct usage example : node createRange.js 1 5 `);
    process.exit(0);
}

const startNum = Number(process.argv[2]);
const endNum = Number(process.argv[3]);
if (!Number.isInteger(startNum) || !Number.isInteger(endNum)) {
    console.error("Please usage integer arguments. Correct usage example : node createRange.js 1 5 ");
    process.exit(0);
}

const outputArray = [];
let j =0;
if (startNum < endNum) {
    for (let i=startNum; i<=endNum; i++) { // ascending order
        outputArray[j++] = i;
    }
}
else if (startNum === endNum) {
    outputArray[0] = startNum;
}
else {
    for (let i=startNum; i>=endNum; i--) { // descending order
        outputArray[j++] = i;
    }
}

console.log(outputArray);
