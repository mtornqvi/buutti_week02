/** 
 * Create program that outputs competitors placements with following way: ['1st competitor was
Julia', '2nd competitor was Mark', '3rd competitor was Spencer', '4th competitor
was Ann', '5th competitor was John', '6th competitor was Joe']
 */



const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinalSuffixes = ["st", "nd", "rd", "th"];

const outputArray = [];

for (let i=0; i < competitors.length; i++) {
    const ordinalSuffix = i < 4 ? ordinalSuffixes[i] : ordinalSuffixes[3];  // choose correct ordinal suffix : 1st, 2nd, 3rd, 4th, 5th...
    outputArray[i] = (i+1) + ordinalSuffix + " competitor was " + competitors[i];
}

console.log(outputArray);
