/** 
 * Randomize order of array
const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
Create a program that every time you run it, prints out an array with differently randomized order of the
array above.
Example:
node .\randomizeArray.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]
 */

function getRandomInt(min,max) {
    return Math.round(min + Math.random() * (max-min));
}

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];


// Create an array which dictates the order based on which values from original array are picked
const orderArray = [];
let counter = 0;
while (orderArray.length < array.length) {
    let num = getRandomInt(0,array.length-1);
    if (!orderArray.includes(num)) { // no repeating order numbers
        orderArray[counter++] = num;
    }
}

const randomArray = []; 
for (let i=0; i < array.length; i++) {
    randomArray[i] = array[orderArray[i]];
}

console.log(randomArray);