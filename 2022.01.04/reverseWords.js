/** Create a programs that reverses each word in a string.
node .\reverseWords.js "this is a very long sentence" -> sihT si a yrev gnol
ecnetnes
 */


const n = process.argv.length;
if (n !== 3) {
    console.error(`Incorrect amount of arguments : ${n-2}. Correct usage example : node reverseWords.js "this is a very long sentence". Please use brackets. `);
    process.exit(0);
}

const wordArray = process.argv[2].split(" ");
// note : must first split a word string to a word array so it can be processed  
const reversedWordArray = wordArray.map(word => word.split("").reverse().join(""));
const outputString = reversedWordArray.join(" ");
console.log(outputString);