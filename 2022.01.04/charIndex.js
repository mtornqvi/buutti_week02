/** Create a program that turns any given word into charIndex version of the word
 ** Example:
 ** node .\charIndex.js "bead" -> 2514
 ** node .\charIndex.js "rose" -> 1815195
 */


const n = process.argv.length;
if (n !== 3) {
    console.error(`Incorrect amount of arguments : ${n-2}. Correct usage example : node .\charIndex.js bead. `);
    process.exit(0);
}

const inputStr = process.argv[2].toUpperCase(); // convert to upper case, unicode starts at A = 0034
const testString = "AZ"; // only handle ascii characters
const lowerLimit = testString.charCodeAt(0);
const upperLimit = testString.charCodeAt(1);

let outputStr = "";
for (let i=0; i< inputStr.length; i++) {
    let charCode = inputStr.charCodeAt(i);
    if (charCode < lowerLimit || charCode > upperLimit) {
        console.error("Please use only ASCII characters (A-Z). Correct usage example : node .\charIndex.js bead. ");
        process.exit(0);
    }

    let num = charCode - lowerLimit + 1; // adjust the number to that A=1 (also a=1, converted to upper case)
    outputStr = outputStr + num.toString();
}



console.log(outputStr);