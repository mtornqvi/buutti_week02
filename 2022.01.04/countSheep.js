/** Create a program that takes in a number from the command line, for example node .\countSheep.js 3
 ** and prints a string "1 sheep...2 sheep...3 sheep..."
 */


const n = process.argv.length;
if (n !== 3) {
    console.error(`Incorrect amount of arguments : ${n-2}. Correct usage example : node countsheep.js 6 `);
    process.exit(0);
}

const sheepCount = Number(process.argv[2]);
if (!Number.isInteger(sheepCount)) {
    console.error("Please usage integer argument. Correct usage example : node countsheep.js 6 ");
    process.exit(0);
}

let outputString = "";
for (let i=1;i<=sheepCount;i++) {
    outputString = outputString + i + " sheep...";
}

console.log(outputString);

